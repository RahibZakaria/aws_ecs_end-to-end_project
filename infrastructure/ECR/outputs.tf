output "ecr_repository_url" {
  value = aws_ecr_repository.docker_image_repo.repository_url
}