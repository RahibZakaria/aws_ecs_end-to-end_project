variable "bastion-instance-type" {
  default = "t2.micro"
}

variable "webserver-instance-type" {
  default = "t2.medium"
}

variable "container-port" {
  default = 5000
}

variable "host-port" {
  default = 5000
}

#variable "image" {
#  default = "492700798506.dkr.ecr.us-east-1.amazonaws.com/aws_ecs_end-to-end_project:latest"
#}

variable "environment" {
  default = "stable"
}

variable "cluster_name" {
  default = "end-to-end-ecs-project-cluster"
}

# for docker image and tag
variable img_tag {}
variable ecr_repo {}