# AWS ECS end-to-end project automated with terraform and gitlab ci  

The goal of this project is to make an end-to-end CI/CD pipeline and deploy AWS ECS services using terraform.

## Infrastructure architecture

![infrastructure](/diagrams/infrastructure.jpeg)


## AWS services used:
1. S3 bucket for storing and managing Terraform state
2. ECS for handling containers overall
3. Application Load Balancer 
4. Auto-scaling groups 
5. EC2 instances as ECS compute resource
6. EC2 bastion host to ssh into ECS EC2 instances for updating or patching
7. Internet gateway to give access to the internet
8. NAT gateway to allow ECS EC2 instances placed in private subnet access to internet 


